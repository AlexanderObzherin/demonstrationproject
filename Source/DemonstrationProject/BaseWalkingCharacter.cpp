// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseWalkingCharacter.h"

// Sets default values
ABaseWalkingCharacter::ABaseWalkingCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABaseWalkingCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseWalkingCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseWalkingCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ABaseWalkingCharacter::CalculateDead()
{
	if( Health <= 0 )
	{
		isDead = true;
	}
	else
	{
		isDead = false;
	}
}

void ABaseWalkingCharacter::CalculateHealth( float delta )
{
	Health += delta;
	CalculateDead();
}

#if WITH_EDITOR
void ABaseWalkingCharacter::PostEditChangeProperty( FPropertyChangedEvent & PropertyChangedEvent )
{
	isDead = false;
	Health = 100;

	Super::PostEditChangeProperty( PropertyChangedEvent );

	CalculateDead();
}
#endif
