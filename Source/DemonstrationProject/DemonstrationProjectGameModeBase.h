// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DemonstrationProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DEMONSTRATIONPROJECT_API ADemonstrationProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
